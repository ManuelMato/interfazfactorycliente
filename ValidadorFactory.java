package simulacion;

//import simulacion.*;

class ValidadorFactory 
{
	public static IValidadorDocumento getValidador(String doc)
	{
		IValidadorDocumento miValidador = null;

		if(doc.length() == 9 && Character.isLetter(doc.charAt(8))
			&& (Character.toUpperCase(doc.charAt(0)) == 'X'
			|| 	Character.toUpperCase(doc.charAt(0)) == 'Y'    
			|| 	Character.toUpperCase(doc.charAt(0)) == 'Z')) {

			miValidador = new ValidadorNIE(doc);

		} else if(doc.length() == 9 && Character.isLetter(doc.charAt(8))) {
			
			miValidador = new ValidadorDNI(doc);
		}
		 	
		 return miValidador;
	}
}

//Al validar aquí las entradas, las elimino de las clases implementadas
