package simulacion;

public class ValidadorNIE implements IValidadorDocumento {
	
	private String nie = null;
	
	public ValidadorNIE(String nie) {
		this.nie = nie;
	}

	public boolean validar() {

		boolean esValido = false;
		int i = 1;
		int caracterASCII = 0;
		char letra = ' ';
		int miNIE = 0;
		int resto = 0;
		char[] asignacionLetra = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X','B', 
								  'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};


		do {
			caracterASCII = this.nie.codePointAt(i);
			esValido = (caracterASCII > 47 && caracterASCII < 58);
			i++;
		} while(i < this.nie.length() - 1 && esValido);
		

		if(esValido && this.nie.substring(0,1).toUpperCase().equals("X")) {
			this.nie = "0" + this.nie.substring(1,9);
		} else if(esValido && this.nie.substring(0,1).toUpperCase().equals("Y")) {
			this.nie = "1" + this.nie.substring(1,9);
		} else if(esValido && this.nie.substring(0,1).toUpperCase().equals("Z")) {
			this.nie = "2" + this.nie.substring(1,9);
		}

		if(esValido) {
			letra = Character.toUpperCase(this.nie.charAt(8));
			miNIE = Integer.parseInt(this.nie.substring(1,8));
			resto = miNIE % 23;
			esValido = (letra == asignacionLetra[resto]);
		}

		return esValido;
	}
}



