package simulacion;

public class ValidadorDNI implements IValidadorDocumento {

	private String dni = null;
	
	public ValidadorDNI(String dni)
	{
		this.dni = dni;
	}

	public boolean validar() {

		boolean esValido = false;
		int i = 0;
		int caracterASCII = 0;
		char letra = ' ';
		int miDNI = 0;
		int resto = 0;
		char[] asignacionLetra = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X',
								  'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};

		do {
			caracterASCII = this.dni.codePointAt(i);
			esValido = (caracterASCII > 47 && caracterASCII < 58);
			i++;
		} while(i < this.dni.length() - 1 && esValido);		

		if(esValido) {
			letra = Character.toUpperCase(this.dni.charAt(8));
			miDNI = Integer.parseInt(this.dni.substring(0,8));
			resto = miDNI % 23;
			esValido = (letra == asignacionLetra[resto]);
		}

		return esValido;
	}
}



