package simulacion;

public class Simulador {

	public static void main(String[] args) {

		String documento = "";
		IValidadorDocumento miValidador = ValidadorFactory.getValidador(documento);

		
		if(miValidador != null && miValidador.validar() == true) { 

			System.out.println("El documento es correcto.");
			
		} else {
			System.out.println("El documento no es correcto.");
		}

		
	}
}


/**
Si documento ="", es decir, longitud cero, y en ValidadorFactory en el NIE validadmos mediante substring(0,1) nos va a dar Excepcion.
por esto voy a pasarlo a char en la posición 0.
*/
